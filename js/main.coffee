$(document).ready(() ->
  carusel 'aside .tours', 3, 250, 'a.l', 'a.r', '.wrapper .tour:eq(0)', 3000, false, 'top'
)

carusel = (block, in_window, width, left, right, wrap, time, points, napr) ->

  th = 0
  max = $(block).find(".wrapper .tour").length - in_window
  $("#{block} li.max").html max + 1
  $("#{block} li.this").html 1
  hover = false

  auto = ->
    to th + 1  unless hover
    setTimeout (->
      auto()
    ), time

  to = (num) ->

    num = max if num < 0
    num = 0 if num > max

    $("#{block} li.this").html num + 1

    if napr is "top"
      $(block).find(wrap).animate
        "margin-top": num * -1 * width
      , 500, ->
        th = num

    if napr is "left"
      $(block).find(wrap).animate
        "margin-left": num * -1 * width
      , 500, ->
        th = num

    if points
      $(block).find("#{points} a").removeClass("active").addClass("passive")
      $(block).find("#{points} a:eq(#{num})").removeClass("passive").addClass("active")

  setTimeout (->
    auto()
  ), time

  $(block).hover (->
    hover = true
  ), ->
    hover = false

  $(block).find(left).click ->
    to th - 1
    false

  $(block).find(right).click ->
    to th + 1
    false

  if points
    $(block).find("#{points} a").click ->
      n = $(this).prevAll().length
      to n
      false