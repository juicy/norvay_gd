<?php include 'kit/head.php' ?>
<?php include 'kit/header.kit' ?>
<div class='content'>
  <div class='wrap'>
    <section>
      <?php foreach ($index_articles as $article): ?>
        <?php require 'kit/article.php' ?>
      <?php endforeach ?>
      <?php if (sizeof($index_articles) == 0): ?>
        <article><p class="none">Статей не найдено</p></article>
      <?php endif ?>
      <?php // include 'kit/pagination.kit' ?>
    </section>
    <?php include 'kit/aside.kit' ?>
    <div class='clear'></div>
  </div>
</div>
<?php include 'kit/footer.kit' ?>
<?php include 'kit/foot.kit' ?>