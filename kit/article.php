<article>
  <div class="stats">
    <p class="name"><?php echo $article['name'] ?></p>
    <p class="user"><span></span><?php echo $article['nickname'] ?></p>
    <p class="date"><?php echo $article['date'] ?></p>
    <div class="categories">
      <?php foreach ($article['categories'] as $article_category_id): ?>
        <?php foreach ($categories as $category): ?>
        <?php if ($article_category_id == $category['id']): ?>
          <a title="<?php echo $category['title'] ?>" href="<?php echo category_url($category['id']) ?>" class="type_<?php echo $category['id'] ?>"></a>
        <?php endif ?>
        <?php endforeach ?>
      <?php endforeach ?>
    </div>
  </div>
  <div class="data">
    <h3><a href="<?php echo article_url($article['id']) ?>"><?php echo $article['title'] ?></a></h3>
    <?php echo $article['anounce'] ?>
    <p class="move"><a href="<?php echo article_url($article['id']) ?>">читать статью</a></p>
  </div>
  <div class="clear"></div>
</article>