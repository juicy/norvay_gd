<?php include 'kit/head.php' ?>
<?php include 'kit/header.kit' ?>
<div class='content'>
  <div class='wrap'>
    <section>
      <?php include 'kit/big_article.kit' ?>
    </section>
    <?php include 'kit/aside.kit' ?>
    <div class='clear'></div>
  </div>
</div>
<?php include 'kit/footer.kit' ?>
<?php include 'kit/foot.kit' ?>