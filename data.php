<?php

# links & texts on site
$links = array(
  'jazz' => '#',
  'menu_home' => 'http://spec.gooddays.ru/jazz_norway_2013/',
  'menu_jazz' => 'http://www.jazztour.ru/',
  'menu_about' => 'http://www.jazztour.ru/norway/',
  'menu_tours' => 'http://www.jazztour.ru/tours/norway/',
  'phone' => '8 (495) 775-76-76',
  'phone_link' => 'callto://84957757676',
  'h1' => 'Большая прогулка: <span>Северная Норвегия</span>',
  'p' => 'О путешествии по Северной Норвегии без наличных денег, о Капитане Фрэде и рыбалке на тошка, о плантациях морошки и её хозяевах, интервью с шеф-поварами ресторанов за Полярным кругом и белоснежные пляжи Вестералена, узнаете о жизни простых норвежцев, совершите вместе с нами прогулку на одном из лайнеров Хуртигрутен по фьордам и хайкинг в горах Свольвера…',
  'copyright' => '2013 © GoodDays.ru спецпроект <br> Все права защищены',
  'banner' => 'http://www.jazztour.ru/tours/norway/',
);

# categories of articles ↓
$categories = array(
  array('id' => 2,  'title' => 'хайкинг'),
  array('id' => 3,  'title' => 'рыбалка'),
  array('id' => 4,  'title' => 'рестораны'),
  array('id' => 5,  'title' => 'интервью'),
  array('id' => 6,  'title' => 'природа'),
  array('id' => 7,  'title' => 'музеи'),
  array('id' => 8,  'title' => 'уличный стрит'),
  array('id' => 9,  'title' => 'лайнер'),
  array('id' => 10, 'title' => 'виды'),
  array('id' => 11, 'title' => 'фуникулер')
);

# links for partners in aside ↓
$partners = array(
  'http://www.visitnorway.com/',  # norway
  'http://www.nordnorge.com/en',  # northers
  'http://www.hurtigruten.com/'   # hurtigruten
);

# tours ↓
$tours = array(
  array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/31560.jpg',
    'title' => 'Загадки Норвегии: Фьорды ',
    'price' => 1295,
    'days' => '8 дней / 7 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
  array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/99186.jpg',
    'title' => 'Норвежская Лапландия - Сказочные снеговики в краю Северного Сияния ',
    'price' => 995,
    'days' => '7 дней / 6 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
  array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/23536.jpg',
    'title' => 'Скандинавские жемчужины: Столицы и Фьорды',
    'price' => 1775,
    'days' => '10 дней / 9 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
  array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/19307.jpg',
    'title' => 'Новый год Осло-Копенгаген (с морским круизом) ',
    'price' => 1095,
    'days' => '7 дней / 6 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
  array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/99071.jpg',
    'title' => 'Новый год Копенгаген-Осло-Стокгольм',
    'price' => 1290,
    'days' => '9 дней / 8 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
    array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/99058.jpg',
    'title' => 'Рождество Копенгаген-Осло (с морским круизом) ',
    'price' => 1095,
    'days' => '7 дней / 6 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
    array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/22424.jpg',
    'title' => 'Скандинавские миниатюры: Берген - отдых на фьордах - Осло ',
    'price' => 1190,
    'days' => '8 дней / 7 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
    array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/137508.jpg',
    'title' => 'Скандинавские миниатюры: Олесунд - Берген - Осло',
    'price' => 1345,
    'days' => '8 дней / 7 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
    array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/20233.jpg',
    'title' => 'Неделя на Фьордах 1 ',
    'price' => 570,
    'days' => '8 дней / 7 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
    array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/51031.jpg',
    'title' => 'Неделя на Фьордах 2 ',
    'price' => 575,
    'days' => '9 дней / 8 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
    array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/20224.jpg',
    'title' => 'Ривьера и фьорды Норвегии ',
    'price' => 625,
    'days' => '9 дней / 8 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
        array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/48170.jpg',
    'title' => 'Норвежские фьорды ',
    'price' => 715,
    'days' => '10 дней / 9 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
            array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/121539.jpg',
    'title' => 'Тромсе - Лофотены ',
    'price' => 1100,
    'days' => '8 дней / 7 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
                array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/121496.jpg',
    'title' => 'Тромсе - Норд Кап',
    'price' => 1150,
    'days' => '8 дней / 7 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
                    array(
    'img' => 'http://www.jazztour.ru/images/sizes/223x167/48185.jpg',
    'title' => 'Круиз: с Севера на Юг Киркенес - Берген (лето 15.04-14.09) ',
    'price' => 1749,
    'days' => '6 дней / 5 ночей',
    'link' => 'http://www.jazztour.ru/tours/norway/ny_ski/'
  ),
);

# articles ↓, full text of article in file /full/$id.kit
$articles = array(
  array(
    'id' => 1,
    'title' => 'Большая прогулка: Северная Норвегия. Фотоальбом',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/nordnorge1-600x400.jpg">
      <p>Фотоальбом по мотивам проекта «Большая прогулка: Северная Норвегия» в рамках которого Виталий Володин и Андрей Железинский совершили увлекательный трип по Северной Норвегии, по маршруту: Тромсё-Сенья-Вестеролен-Лофотены. Вы увидите удивительные и интересные фотографии рассказывающие о ...</p>
    ',
    'aside_anounce' => 'Фотоальбом по мотивам проекта «Большая прогулка: Северная Норвегия» в рамках которого ...',
    'name' => 'Виталий <span>Володин</span>',
    'nickname' => '<a href="">dewald</a>',
    'date' => '02 сентября 2013',
    'categories' => array(2, 3),
    'full' => '' 
  ),
  array(
    'id' => 2,
    'title' => 'Большая прогулка: Северная Норвегия. Первый почтовый на Север',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Hurtigruten7-600x298.jpg">
      <p>В финале романа Жюля Верна «Двадцать тысяч лье под водой» герои, чудом выжившие в Мальстрёме, проводят два месяца на Лофотенах в ожидании корабля на юг. Великий фантаст мог вообразить ужасающую картину водоворота «такой ...</p>
    ',
    'aside_anounce' => 'В финале романа Жюля Верна «Двадцать тысяч лье под водой» герои, чудом выжившие...',
    'name' => 'Виталий <span>Володин</span>',
    'nickname' => '<a href="">dewald</a>',
    'date' => '29 августа 2013',
    'categories' => array(9, 6),
    'full' => ''
  ),
    array(
    'id' => 3,
    'title' => 'Большая прогулка. Северная Норвегия: Умный? В гору!',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Svolvaer-Tuva-600x400.jpg">
      <p>После незабываемой рыбалки в Å у нас по плану было протестировать ещё одно любимое норвежское развлечение — хайкинг или горный туризм. Собственно, любая загородная прогулка в стране, где гор больше, чем жителей, неизбежно ...</p>
    ',
    'aside_anounce' => 'После незабываемой рыбалки в Å у нас по плану было протестировать ещё...',
    'name' => 'Виталий <span>Володин</span>',
    'nickname' => '<a href="">dewald</a>',
    'date' => '27 августа 2013',
    'categories' => array(8, 6, 2),
    'full' => ''
  ),
     array(
    'id' => 4,
    'title' => 'Большая прогулка: Северная Норвегия. Hurtigruten',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Hurtigruten6-600x401.jpg">
      <p>Они не спеша подтягивались на причал, разные, синие, белые, красные, бородатые с рюкзаками и вальяжные с чемоданами… лайнер ещё не появился, а пассажиров на уютных скамейках скопилось прилично. Никто не хотел пропустить момент ...</p>
    ',
    'aside_anounce' => 'Они не спеша подтягивались на причал, разные, синие, белые, красные, бородатые с рюкзаками и...',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '27 августа 2013',
    'categories' => array(6, 9, 4, 5),
    'full' => ''
  ),
     array(
    'id' => 5,
    'title' => 'Большая прогулка: Северная Норвегия. Свольвер II',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Svolvaer2-600x401.jpg">
      <p>Увидеть норвежские фьорды с берега и с моря, всласть порыбачить, посмотреть северное сияние или окунуться в белые ночи и, даже отведать морошкового варенья и поколесить по Норвегии на байке… да, эта страна создана ...</p>
    ',
    'aside_anounce' => 'Увидеть норвежские фьорды с берега и с моря, всласть порыбачить, посмотреть ...',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '25 августа 2013',
    'categories' => array(6, 8, 4, 5, 2),
    'full' => ''
  ),
     array(
    'id' => 6,
    'title' => 'Большая прогулка. Северная Норвегия: Рыба моей мечты',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/fishing-Lofoten-600x393.jpg">
      <p>Мудрецы прошлого говорили: хочешь накормить человека один раз — дай ему рыбу. Хочешь на всю жизнь — научи его рыбачить. Держа в голове эти, без всякого сомнения очень точные и справедливые слова, мы ...</p>
    ',
    'aside_anounce' => 'Мудрецы прошлого говорили: хочешь накормить человека один раз — дай ему рыбу. Хочешь на всю жизнь — научи...',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '25 августа 2013',
    'categories' => array(3, 6, 4, 2),
    'full' => ''
  ),
     array(
    'id' => 7,
    'title' => 'Большая прогулка: Северная Норвегия. Город рорбю',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Svolvaer-600x391.jpg">
      <p>Однажды давным-давно предкам нынешних норвежцев наскучило круглый год бегать на лыжах наперегонки с белыми медведями. Тогда они решили посмотреть мир, став первой в истории нацией туристов. Северяне на круизных драккарах произвели на европейцев ...</p>
    ',
    'aside_anounce' => 'Однажды давным-давно предкам нынешних норвежцев наскучило круглый год бегать на лыжах наперегонки с белыми медведями',
    'name' => 'Виталий <span>Володин</span>',
    'nickname' => '<a href="">dewald</a>',
    'date' => '24 августа 2013',
    'categories' => array(8, 6, 4),
    'full' => ''
  ),
     array(
    'id' => 8,
    'title' => 'Большая прогулка: Северная Норвегия. Деревушка А',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Lofoten-600x382.jpg">
      <p>Лофотенские острова переворачивают твоё сознание, и если раньше северная природа казалась тебе неким сомнительным удовольствием, то Лофотены влюбляют в себя навсегда. Влюбляют настолько сильно, что по возвращении московская непогода выглядит лёгким флиртом. Непогода ... </p>
    ',
    'aside_anounce' => 'Лофотенские острова переворачивают твоё сознание, и если раньше северная природа казалась тебе неким сомнительным удовольствием...',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '24 августа 2013',
    'categories' => array(6, 4, 3),
    'full' => ''
  ),
     array(
    'id' => 9,
    'title' => 'Большая прогулка: Северная Норвегия. Свольвер I',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Svolver1-600x401.jpg">
      <p>Пятое утро Большой прогулки по Северной Норвегии, выдалось для нас хмурым. Покидая ранним утром Фьордкамп, мы направлялись в Сортланд, чтобы пересесть на автобус и оказаться в Свольвере. Как и положено погоде на Лофотенских ...</p>
    ',
    'aside_anounce' => 'Пятое утро Большой прогулки по Северной Норвегии, выдалось для нас хмурым. Покидая ранним утром Фьордкамп, мы направлялись в...',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '22 августа 2013',
    'categories' => array(6, 8, 4, 5),
    'full' => ''
  ),
     array(
    'id' => 10,
    'title' => 'Большая прогулка: Северная Норвегия. На берегу',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Fjordcamp3-600x362.jpg">
      <p>Угостившись морошкой и поглазев на тюленей, мы отправились дальше по норвежскому архипелагу Вестеролен. Выйдя из машины на очередной фотостоп… всё-таки она была прирожденная позёрка. Ну вот какой смысл вот так торчать у всех ... </p>
    ',
    'aside_anounce' => 'Угостившись морошкой и поглазев на тюленей, мы отправились дальше по норвежскому архипелагу Вестеролен...',
    'name' => 'Виталий <span>Володин</span>',
    'nickname' => '<a href="">dewald</a>',
    'date' => '20 августа 2013',
    'categories' => array(3, 6, 4, 2),
    'full' => ''
  ),
     array(
    'id' => 11,
    'title' => 'Большая прогулка: Северная Норвегия. Конец света на краю света',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Vesteraalen-600x330.jpg">
      <p>Мало кто знает, что 25 января 1995 года мир оказался на пороге ядерной войны. Инцидент, который принес немало седых волос российским и американским генералам, начался в поселке Анденес, что на заполярном архипелаге Вестеролен ...</p>
    ',
    'aside_anounce' => 'Мало кто знает, что 25 января 1995 года мир оказался на пороге ядерной войны. Инцидент, который...',
    'name' => 'Виталий <span>Володин</span>',
    'nickname' => '<a href="">dewald</a>',
    'date' => '20 августа 2013',
    'categories' => array(6, 4),
    'full' => ''
  ),
     array(
    'id' => 12,
    'title' => 'Большая прогулка: Северная Норвегия. Фьордкамп',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Fjordcamp4-600x401.jpg">
      <p>Мы подъезжали к Fjordcamp, небо хмурилось, как бы говоря: «Хватит ребята, и так фору в три солнечных дня вы уже получили». Но дождь во фьордах… это по-своему красиво. На горы шапкой садится туман ...</p>
    ',
    'aside_anounce' => 'Мы подъезжали к Fjordcamp, небо хмурилось, как бы говоря: «Хватит ребята, и...',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '18 августа 2013',
    'categories' => array(6, 3, 4, 2),
    'full' => ''
  ),
     array(
    'id' => 13,
    'title' => 'Большая прогулка: Северная Норвегия. Вестеролен',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Vesteraalen1-600x401.jpg">
      <p>Покидая остров Сенья, мы первый раз оказались на пароме, который шёл через открытое море, направляясь из деревушки Hamn-Gryllefjord на архипелаг Вестеролен. Зелёный цвет палубы напоминал игровой стол, на который высыпали горсть мячиков для ...</p>
    ',
    'aside_anounce' => '',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '18 августа 2013',
    'categories' => array(6, 3, 4, 7, 2),
    'full' => ''
  ),
     array(
    'id' => 14,
    'title' => 'Большая прогулка: Северная Норвегия. Никогда не разговаривайте с троллями!',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Senja-600x400.jpg">
      <p>Норвежцы насчитали в своей стране 50 тысяч островов. Ну или около того, при таких масштабах немудрено сбиться со счета. У Робинзона, если бы его занесла нелегкая в эти края, был бы отличный выбор. ... </p>
    ',
    'aside_anounce' => 'Норвежцы насчитали в своей стране 50 тысяч островов. Ну или около того...',
    'name' => 'Виталий <span>Володин</span>',
    'nickname' => '<a href="">dewald</a>',
    'date' => '17 августа 2013',
    'categories' => array(8, 7, 6, 2),
    'full' => ''
  ),
     array(
    'id' => 15,
    'title' => 'Большая прогулка: Северная Норвегия. Сенья',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Senja1-600x401.jpg">
      <p>По дороге, отправляясь из Тромсё на остров Сенья и далее на Вестерален и Лофотенские острова, мы перемещались и на паромах, посещали колоритные рыбацкие деревушки, делали фотостопы на белоснежных пляжах. Надо отметить, что мы ...</p>
    ',
    'aside_anounce' => 'По дороге, отправляясь из Тромсё на остров Сенья и далее на Вестерален и Лофотенские острова',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '15 августа 2013',
    'categories' => array(6, 3, 7, 5, 2),
    'full' => ''
  ),
     array(
    'id' => 16,
    'title' => 'Большая прогулка: Северная Норвегия. Как сделать тролля',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Tromso7-600x400.jpg">
      <p>Чем хитрее твой противник, тем проще должна быть ловушка, которую ты ему готовишь. Вот как бывает — просыпаешься с утра в прекрасном настроении, а потом день не задался — и ты уже экспонат ...</p>
    ',
    'aside_anounce' => 'Чем хитрее твой противник, тем проще должна быть ловушка, которую ты ему готовишь. Вот как бывает...',
    'name' => 'Виталий <span>Володин</span>',
    'nickname' => '<a href="">dewald</a>',
    'date' => '15 августа 2013',
    'categories' => array(8, 7),
    'full' => ''
  ),
     array(
    'id' => 17,
    'title' => 'Большая прогулка: Северная Норвегия. Тромсё. Часть 3',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Tromso11-600x401.jpg">
      <p>В Тромсё всё рядом и выходя из Полярного музея, сразу попадаешь на центральную улицу Тромсё под названием Storgata, в её пешеходную зону, вначале которой находится небольшая площадь. Календарный понедельник, но, не смотря на ...</p>
    ',
    'aside_anounce' => 'В Тромсё всё рядом и выходя из Полярного музея, сразу попадаешь на центральную улицу...',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '14 августа 2013',
    'categories' => array(8, 7, 4, 5),
    'full' => ''
  ),
     array(
    'id' => 18,
    'title' => 'Большая прогулка: Северная Норвегия. Цветущий Север',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Tromso-garden-600x400.jpg">
      <p>19 минут северной широты отделяют норвежский Тромсё от сибирского Норильска. Сущая безделица — меньше сорока километров. Но именно они позволяют Тромсё гордо считаться самым северным из крупных городов мира. Дальше вплоть до самого ...</p>
    ',
    'aside_anounce' => '19 минут северной широты отделяют норвежский Тромсё от сибирского Норильска...',
    'name' => 'Виталий <span>Володин</span>',
    'nickname' => '<a href="">dewald</a>',
    'date' => '13 августа 2013',
    'categories' => array(8, 6, 7),
    'full' => ''
  ),
     array(
    'id' => 19,
    'title' => 'Большая прогулка: Северная Норвегия. Тромсё. Часть 2',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Tromso9-600x401.jpg">
      <p>Увидев вживую голубые маки и изучив Арктический собор, мы вернулись в центр Тромсё, так как всё что мы хотели посетить в этот день, находилось в шаговой доступности в периметре центральной улицы Storgata. Просто ...</p>
    ',
    'aside_anounce' => 'Увидев вживую голубые маки и изучив Арктический собор, мы вернулись в...',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '13 августа 2013',
    'categories' => array(8, 7, 4),
    'full' => ''
  ),
     array(
    'id' => 20,
    'title' => 'Большая прогулка: Северная Норвегия. Тромсё. Часть 1',
    'anounce' => '
      <img src="http://blog.jazztour.ru/wp-content/uploads/Tromso10-600x401.jpg">
      <p>Уютно и слегка нагло расположившись в кожаном кресле, так как мне удобно, я наблюдал за ним. Мне было интересно в нём ВСЁ. Мне говорили, что я могу опоздать к нему на встречу или, ...</p>
    ',
    'aside_anounce' => 'Уютно и слегка нагло расположившись в кожаном кресле, так как мне удобно...',
    'name' => 'Андрей <span>Железинский</span>',
    'nickname' => '<a href="">jazztour</a>',
    'date' => '12 августа 2013',
    'categories' => array(8, 7),
    'full' => ''
  ),
     array(
    'id' => 21,
    'title' => 'Большая прогулка: Северная Норвегия. Трейлер',
    'anounce' => '
      <iframe src="//player.vimeo.com/video/71951983?portrait=0&amp;autoplay=0" width="600" height="401" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      <p>В рамках проекта «Большая прогулка», Виталий Володин и Андрей Железинский совершили увлекательный трип по Северной Норвегии, по маршруту: Тромсо-Сенья-Вестерален-Лофотены...</p>
    ',
    'aside_anounce' => 'В рамках проекта «Большая прогулка», Виталий Володин и Андрей Железинский совершили увлекательный...',
     'nickname' => 'Jazz Tour',
    'date' => '10 августа 2013',
    'categories' => array(),
    'full' => ''
  ),
);

#!!! DONT TOUCH ↓ !!!#

$last_articles = array_slice($articles, 0, 3, true);

if (isset($_GET['category_id'])) {
  $id = $_GET['category_id'];
  switch ($id) {
    case 'all':
      $index_articles = $articles;
      break;
    default:
      $index_articles = array();
      foreach ($articles as $article) {
        foreach ($article['categories'] as $category) {
          if ($category == $id) {
            array_push($index_articles, $article);
          }
        }
      }
      break;
  }
} else {
  $index_articles = $articles;
}

if (isset($_GET['article_id'])) {
  $id = $_GET['article_id'];
  foreach ($articles as $article) {
    if ($article['id'] == $id) {
      $inner_article = $article;
      $inner_article['full'] = file_get_contents("full/$id.kit");
    }
  }
  if (isset($inner_article)) {
    $article = $inner_article;
  } else {
    $article = false;
  }
}

function category_url($id = 'all') {
  return "/jazz_norway_2013/index.php?category_id=$id";
}

function article_url($id) {
  return "/jazz_norway_2013/inner.php?article_id=$id";
}

?>